#!/usr/bin/env bash
#
# Create the USB key contents (run locally on a Mac).

set -euxo pipefail

# Script directory
dir=$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd -P)

# Location to build the USB files
readonly usbdir="${dir}/usb"
mkdir -p "${usbdir}"

# Docker image name
version=$(sed -e 's/^[ \t]*//' < "${dir}"/VERSION)
docker_img="lancelet/hs-spaceflight-2019:${version}"

# Make Docker image and save it
./build.sh
docker save "${docker_img}" | \
    gzip --best -c > "${usbdir}/hs-spaceflight-2019.img.gz"

# Bundle the git repo
git clone https://github.com/lancelet/space-workshop.git
pushd space-workshop
git bundle create "${usbdir}/space-workshop.gitarchive" --all
popd
rm -rf space-workshop

# Copy the notes and slides
curl https://lancelet.github.io/space-workshop/notes.pdf \
     > "${usbdir}/notes.pdf"
curl https://lancelet.github.io/space-workshop/intro-slides.pdf \
     > "${usbdir}/intro-slides.pdf"

# Copy the README file
cp usb-README.md "${usbdir}/README.md"
