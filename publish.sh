#!/bin/sh
dir=$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd -P)
version=$(sed -e 's/^[ \t]*//' < "${dir}"/VERSION)
echo -n ${DOCKER_HUB_PASSWORD} | docker login -u lancelet --password-stdin
docker push lancelet/hs-spaceflight-2019:"${version}"
