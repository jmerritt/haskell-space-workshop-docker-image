#!/bin/sh
dir=$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd -P)
version=$(sed -e 's/^[ \t]*//' < "${dir}"/VERSION)
docker build -t lancelet/hs-spaceflight-2019:"${version}" hs-spaceflight-2019
