# USB Key Instructions

First, copy everything off the USB key to the local filesystem.

## Loading the Docker image

The image can be loaded into Docker as follows:

```
gunzip -c hs-spaceflight-2019.img.gz | docker load
```

## Un-archiving the GitHub repo

To un-archive the GitHub repository (ie. take the Git bundle and create a Git repository):

```
git clone space-workshop.gitarchive space-workshop
```

## Development from a Local Directory

You can use your favourite text editor, and still compile using the Docker image:

```
cd <your-location-of-space-workshop-git-checkout>
docker run --rm -v $(pwd):/space-workshop -ti --cpus 4 lancelet/hs-spaceflight-2019:0.1.0 bash
```

## Linux Ownership Issues and `stack ghci`

If running `ghci` from inside Docker on Linux, to overcome permission issues, run it with:

```
stack repl --allow-different-user --ghci-options '-ghci-script .ghci'
```

## Development from a Local Directory using `wshterm`

If you do not use `iTerm`, and you want to view images in `ghci` (using `plotXXX Screen` commands from `ghci`), one option is `wshterm`:

```
cd <your-location-of-space-workshop-git-checkout>
docker run --rm -v $(pwd):/space-workshop -p 8080:8080 -ti --cpus 4 lancelet/hs-spaceflight-2019:0.1.0 stack exec wshterm -- -p 8080 bash
```

Then visit [localhost:8080](http://localhost:8080) for the browser-based terminal.

## Running Hoogle 

You can run Hoogle from the Docker image to view bundled documentation for all the packages we use:

```
docker run --rm -p 8081:8080 -ti lancelet/hs-spaceflight-2019:0.1.0 bash -c 'cd /opt/space-workshop && stack hoogle server -- -p 8080'
```

Then visit [localhost:8081](http://localhost:8081) for Hoogle documentation.

You can use `Ctrl-C` in the Docker terminal to quit.
